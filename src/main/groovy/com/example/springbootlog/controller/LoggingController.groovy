package com.example.springbootlog.controller

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController


@RestController
class LoggingController {
    Logger logger = LoggerFactory.getLogger(LoggingController.class)

    @RequestMapping("/")
    public String index() {
        logger.trace("Trace message")
        logger.debug("Debug message")
        logger.info("Info message")
        logger.warn("Warn message")
        logger.error("error message")

        return "Test logging!"
    }

}
