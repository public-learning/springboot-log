package com.example.springbootlog.controller

import com.example.springbootlog.entity.Product
import org.springframework.context.annotation.Bean
import org.springframework.context.support.ResourceBundleMessageSource
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping

import java.time.LocalDateTime

@Controller
class WebController {

    @GetMapping('/index')
    String getIndexTable(Model model) {  //@RequestParam(name="name", required=false, defaultValue="World") String name
        def products = [new Product(1L, "Star shaped baby sleeping bag", "https://www.etsy.com/listing/1346164491", 15.6),
                        new Product(2L, "Silhouette Baby Blanket", "https://www.etsy.com/listing/1332184526", 11.7)]
        model.addAttribute("products", products)
//        model.addAttribute("name", name)

        return "index" //HTML - Template
    }


    @Bean
    ResourceBundleMessageSource messageSource() {
        ResourceBundleMessageSource source = new ResourceBundleMessageSource()
        source.setBasename("message")
        source.setUseCodeAsDefaultMessage(true)
        return source
    }
}
