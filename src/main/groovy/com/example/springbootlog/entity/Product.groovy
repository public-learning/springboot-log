package com.example.springbootlog.entity

import jakarta.persistence.Id
import org.apache.commons.lang3.StringUtils

class Product {
    @Id
    Long id

    String name

    String url

    Double price

    Product(Long id, String name, String url, Double price) {
        this.id = id
        this.name = name
        this.url = url
        this.price = price
    }

    Boolean isBlanket() {
        if (StringUtils.contains(this.name.toLowerCase(), "blanket")) return true
        return false
    }
}
