package com.example.springbootlog

import org.slf4j.LoggerFactory
import org.slf4j.event.Level
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

import java.util.logging.Logger

@SpringBootApplication
class SpringbootLogApplication {

    static void main(String[] args) {
        SpringApplication.run(SpringbootLogApplication, args)
    }

}
